---
layout: layouts/base.njk
permalink: 404.html
eleventyExcludeFromCollections: true
schema:
  '@type': WebPage
  url: https://dwk.io/404.html
  author: 
    '@type': Person
    name: David W. Keith
    url: https://dwk.io/
---
# Content not found

Go [home](/).
