---
title: Unexpected low battery notifications 
product:
    name: Assure Lock for Andersen Patio Doors with Wi-Fi and Bluetooth
    manufacturer: Yale
    product_url: https://shopyalehome.com/collections/patio-door-locks/products/assure-lock-for-andersen-patio-doors-with-wi-fi-and-bluetooth?variant=39337803743364
type: bug
status: unresolved
---
## Steps to reproduce

1. When batteries fail, replace

### Expected Behavior

No new push notifications about low batteries, app shows batteries not low. No other action or verification needed.

### Actual Behavior

Daily push notifications for low batteries, app shows low batteries when they are not. Homeowner needs to open the app and tell the app that the batteries have been replaced.
