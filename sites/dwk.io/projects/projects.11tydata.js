import { getWebmentions, defaults } from "@chrisburnell/eleventy-cache-webmentions";

export default {
  tags: [
    "projects"
  ],
  layout: "layouts/project.njk",
  ogType: "article",
  permalink: "{{ page.fileSlug }}/",
  schema: {
    "@Type": "CreativeWork",
    licence: "https://creativecommons.org/licenses/by-sa/4.0/",
  },
  eleventyComputed: {
    webmentions: (data) => {
      return getWebmentions({
          ...defaults,
          domain: "https://dwk.io",
          feed: `https://webmention.io/api/mentions.jf2?domain=dwk.io&token=${process.env.WEBMENTION_IO_TOKEN}&per-page=9001`,
          key: "children",
        }, "https://dwk.io/" + data.page.url)
    },
  }
};
