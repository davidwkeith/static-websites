---
title: Swift FractionFormatter
language: Swift
date: 2020-05-07T04:49:24.215Z
hero:
  src: logo.webp
  alt: FractionFormatter Logo, a red square with rounded corners and the glyph "½" centered in white
---

FractionFormatter came out of my frustration with third party apps on Apple
platforms using built up fractions (1/2) rather than the typographically correct
Unicode fractions (½).

|          |                                |
|----------|--------------------------------|
| Package  | [Swift Package Index][package] |
| Source   | [GitLab][source]               |

[package]: https://swiftpackageindex.com/davidwkeith/FractionFormatter
[source]: https://gitlab.com/davidwkeith/fractionformatter
