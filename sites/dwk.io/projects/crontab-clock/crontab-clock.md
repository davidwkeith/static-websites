---
title: Crontab Clock
source: crontab-clock
website: https://crontab.dwk.io
language: JavaScript
date: 2021-10-27T04:49:24.215Z
hero:
  src: logo.png
  alt: macOS's Console.app icon
---

A simple web clock that uses the [Crontab date format](https://crontab.guru) to display the current time.

|          |                                |
|----------|--------------------------------|
| Website  | [Crontab Clock][crontab]       |
| Source   | [GitLab][source]               |

[crontab]: https://crontab.dwk.io
[source]: https://gitlab.com/davidwkeith/static-websites/-/tree/main/sites/crontab.dwk.io