---
title: Pullets Forever
website: https://pulletsforever.com
language: HTML
draft: true
date: 2002-12-23T04:49:24.215Z
hero:
  src: logo.svg
  alt: Pullets Forever's chicken head logo in blue
---

Pullet surprise writing about my life.

|          |                                |
|----------|--------------------------------|
| Website  | [Pullets Forever][website]     |
| Source   | [GitLab][source]               |

[website]: https://pulletsforever.com
[source]: https://gitlab.com/davidwkeith/static-websites/-/tree/main/sites/pulletsforever.com
