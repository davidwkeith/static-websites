import globalMetadata from "../../../data/metadata.js";

export default {
  ...globalMetadata,
  title: "David W. Keith",
  site: "dwk.io",
  url: "https://dwk.io",
  favicon: "/img/memoji.png",
  logo: {
    src: "/img/memoji.png",
    alt: "Memoji of DWK's head",
  },
  description: "Cyber home of dwk",
  showSocialLinks: true,
  cloudflare_insights: {
    token: "142c2cc95d23498eb6bead16115a50f2",
  },
  head_links: [
    ...globalMetadata.head_links,
    {
      // MicroSub https://indieweb.org/Microsub
      rel: "microsub",
      href: "https://aperture.p3k.io/microsub/1",
    },

    {
      // IndieAuth https://indieauth.net
      rel: "authorization_endpoint",
      href: "https://indieauth.com/auth",
    },
    {
      // IndieAuth https://indieauth.net
      rel: "token_endpoint",
      href: "https://tokens.indieauth.com/token",
    },
    {
      // PGP-Signed Comments https://golem.ph.utexas.edu/~distler/blog/archives/000320.html
      rel: "pgpkey",
      type: "application/pgp-keys",
      href: "https://keys.openpgp.org/vks/v1/by-fingerprint/DFE7AF6BB47A3B7580F50C1D2D75DC350399F54C",
    },
  ],
};
