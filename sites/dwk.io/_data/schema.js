import globalSchema from "../../../data/schema.js";

export default {
  ...globalSchema,
  ...globalSchema.author,
  // Remove from Person schema
  language: undefined,
  author: undefined,
  description: "Cyberhome of David W. Keith",
  image: {
    "@type": "ImageObject",
    contentUrl: "https://dwk.io/img/memoji.png",
    caption: "Memoji of DWK's head",
  },
  alumniOf: [
    {
      "@type": "CollegeOrUniversity",
      name: "Cornell College",
      url: "https://cornellcollege.edu/",
    },
    {
      "@type": "HighSchool",
      name: "St. Johnsbury Academy",
      url: "https://stjacademy.org",
    },
  ],
  sameAs: [
    "https://xn--4t8h.dwk.io/@Dwk",
    "https://keybase.io/dwkeith",
    "https://www.facebook.com/davidwkeith",
    "https://www.reddit.com/user/dwkeith",
    "https://gitlab.com/davidwkeith",
    "https://github.com/davidwkeith",
    "https://www.linkedin.com/in/davidwkeith",
  ],

  affiliation: [
    {
      "@type": "Organization",
      name: "Silicon Valley Bicycle Coalition",
      url: "https://bikesiliconvalley.org",
    },
    {
      "@type": "Organization",
      name: "TEALS",
      url: "https://www.microsoft.com/en-us/teals",
    },
  ],
  birthDate: "1978-12-14",
  birthPlace: {
    "@type": "Place",
    name: "Boston, MA",
  },
  owns: [
    {
      "@type": "OwnershipInfo",
      name: "Pullets Forever",
      url: "https://pulletsforever.com",
    },
  ],
  callSign: "N1UEU",
  familyName: "Keith",
  givenName: "David",
  additionalName: "William",
  gender: "Male",
  height: "173 cm",
  nationality: {
    "@type": "Country",
    name: "United States of America",
    url: "https://www.usa.gov",
  },
};

  
  


