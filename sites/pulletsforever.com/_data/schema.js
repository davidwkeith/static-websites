import globalSchema from "../../../data/schema.js";

globalSchema.author.email = "mailto:dwk@pulletsforever.com";

export default {
  ...globalSchema,
  name: "Pullets Forever",
  description: "Pullet surprise writing.",
  image: "https://pulletsforever.com/img/logo.svg",
  url: "https://pulletsforever.com/",
};
