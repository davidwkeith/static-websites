---
title: Markdown Test
eleventyExcludeFromCollections: true
draft: true
---

## Headers

This is an example of different header levels:

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

## Emphasis

You can make text *italic*, **bold**, or ==marked==

## Lists

### Unordered List

- Item 1
- Item 2
- Item 3

### Ordered List

1. First item
2. Second item
3. Third item

## Links

You can create [hyperlinks](http://dwk.io) in Markdown. Footnotes[^1] are also supported, using long names[^long-names].

## Images

![Alt Text](/img/sample.png "Title text")

## iFrame Embeds

@[A video of Iba giving a talk at the Women in Tech Festival 2017](https://www.youtube-nocookie.com/embed/Q2MFQXkDMYo "Women in Tech Festival 2017: The Future of Work - Woman and Machine Symbiosis")

## Code Blocks

Inline: `print("Hello, world!")`

Block

```javascript
/* 
 * An example with *markdown* in the comment block
 */
console.log("Hello, world!:);
```

[^1]: Like this
[^long-names]: This is called `long-names`
