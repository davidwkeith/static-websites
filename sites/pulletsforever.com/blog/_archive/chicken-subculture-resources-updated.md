---
title: Chicken Subculture Resources (Updated)
date: 2008-02-07T05:36:05.157Z
tags: [chickens]
---

This will likely continue to be updated as my definitive list of chicken farmers fighting the system. If you come across any resource not listed here, please email the details to [pulletsforever@pulletsforever.com](mailto:pulletsforever@pulletsforever.com)

* [Duluth City Chickens](http://www.duluthcitychickens.org/)

* [Mad City Chickens](http://www.madcitychickens.com/)

* [Burnsville boy to City Council: Please let me keep my chickens](http://www.twincities.com/2008/09/21/burnsville-boy-to-city-council-please-let-me-keep-my-chickens/)

* [Raising chickens in the heart of the city](http://minnesota.publicradio.org/display/web/2007/06/18/chickencoops/)

* [The Craze for Urban Chicken Farming](http://www.newsweek.com/id/168740/)
