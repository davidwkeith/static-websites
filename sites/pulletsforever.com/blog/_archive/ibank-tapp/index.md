---
title: iBank Tapp
date: 2007-08-13T04:48:26.142Z
tags: [hacks, ios, web]
---
![Bank Tapp Screen Shot](screen-shot.jpg)

**UPDATE:** IGG Software has released [iBank Mobile](http://www.iggsoftware.com/ibankmobile/) for the iPhone. It can be purchased through [iTunes](http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=318802616&mt=8).

iBank.tapp automatically links to the last open iBank file and pulls in the appropriate categories transaction types and accounts. If iBank is already open on your Mac it will default to the currently selected account.

Once loaded just enter the Payee, Category and Amount and iBank will automatically create a new transaction of type POS for today’s date. (You are remembering to enter your receipts right a way, right?)

Tap on the vault icon to see your balance for the currently selected account.

![vault icon](/blog/_archive/ibank-tapp/vault-icon.png)

iBank.tapp.zip (1.1.3)

Requirements:

* [telekinesis iPhone Remote](http://code.google.com/p/telekinesis/)

* [iBank 2.3.13](http://www.iggsoftware.com/ibank/downloads.php)
