---
title: Stay of Execution
date: 2006-03-05T04:47:14.845Z
tags: [chickens]
---
![Ginger the chicken, hiding among the bushes](ginger.jpg)

Today I visited City Hall to get a status update on [PoultryGate](../poultrygate). I learned from the town clerk that city council, after review of my letter decided to table the matter and have the town’s lawyer look over the matter.

This tell me two things: for now I can keep the chickens and the town is spending taxpayer money to have a lawyer look at whether they can legally evict pullets based on the wording of the law they wrote…
