---
title: An Update on Taco Truck Tuesday 🇺🇸
date: 2016-10-26T15:36:37.086Z
tags: [politics]
---

Update: The response has been good, but it is difficult to mobilize a large number of food trucks on short notice without a guaranteed profit. My plan of record is to try a small version of this at the midterm elections, and four years from now come out in full force. This is not about shaming a candidate, it is about making it easier to vote, so the intent was always to be a long term event.

A few hundred people have responded positively to my [Taco Truck Tuesday](/taco-truck-tuesday/) post, which is promising, but not enough to make a difference. Thankfully I have received no negative feedback, which is likely a record on the internet.

I have reached out to many of the following organizations for their support, and would appreciate the hundreds who care to do the same.

[**Moveable Feast (@mvblfeast) | Twitter**
*The latest Tweets from Moveable Feast (@mvblfeast). Quality street food in your neighborhood. San Jose, CA*twitter.com](https://twitter.com/mvblfeast)

[**Roaming Hunger (@RoamingHunger) | Twitter**
*The latest Tweets from Roaming Hunger (@RoamingHunger). Roaming Hunger is the easiest way to find and book over 9,000…*twitter.com](https://twitter.com/roaminghunger)

[**The Food Truck Mafia (@foodtruck_mafia) | Twitter**
*The latest Tweets from The Food Truck Mafia (@foodtruck_mafia). The Food Truck Mafia is an event company that puts on…*twitter.com](https://twitter.com/foodtruck_mafia)

[**Food Truck Fiesta (@foodtruckfiesta) | Twitter**
*The latest Tweets from Food Truck Fiesta (@foodtruckfiesta). A real-time automated DC food truck tracker + commentary!…*twitter.com](https://twitter.com/foodtruckfiesta)

[**LA Food Trucks (@lafoodtruck) | Twitter**
*The latest Tweets from LA Food Trucks (@lafoodtruck). LA's Food Truck Twitter and Blog established in 2009 and still…*twitter.com](https://twitter.com/lafoodtruck)

[**the str[EATS] DC (@thestrEATSdc) | Twitter**
*The latest Tweets from the str[EATS] DC (@thestrEATSdc). Just because a restaurant is on wheels doesn't mean it shouldn…*twitter.com](https://twitter.com/thestrEATSdc)

[**cartattack (@cartattackhq) | Twitter**
*The latest Tweets from cartattack (@cartattackhq). A Portland #foodcart finder. Tracking over 400 #PDX food carts…*twitter.com](https://twitter.com/cartattackhq)

[**On The Lot (@OnTheLotLA) | Twitter**
*The latest Tweets from On The Lot (@OnTheLotLA). Presenting gourmet food trucks On The Lot weekly for dinner on Mondays…*twitter.com](https://twitter.com/OnTheLotLA)

[**Cindy Peters (@UniFoodTrucks) | Twitter**
*The latest Tweets from Cindy Peters (@UniFoodTrucks). Following the food trucks that park for weekday lunch on…*twitter.com](https://twitter.com/UNIFOODTRUCKS)

[**Food Truck Army Inc. (@foodtruckarmy) | Twitter**
*The latest Tweets from Food Truck Army Inc. (@foodtruckarmy). Los Angeles #1 Gourmet Foodtruck agency since 2009…*twitter.com](https://twitter.com/foodtruckarmy)

[**Atomic Eats (@AtomicEats) | Twitter**
*The latest Tweets from Atomic Eats (@AtomicEats). Weekly Gourmet Food Truck events in Los Angeles and Orange Counties…*twitter.com](https://twitter.com/AtomicEats)

[**The Truck Stop (@The_Truck_Stop) | Twitter**
*The latest Tweets from The Truck Stop (@The_Truck_Stop). Northwest corner of Beverly Blvd. & La Brea Ave. Hollywood, CA*twitter.com](https://twitter.com/The_Truck_Stop)

[**Chatsworth Din Din (@ChatsworthDDaGG) | Twitter**
*The latest Tweets from Chatsworth Din Din (@ChatsworthDDaGG). Every Tuesday Gourmet Food trucks gather from 5:30p-9 at…*twitter.com](https://twitter.com/ChatsworthDDaGG)

[**Cluster Truck (@clustertruck) | Twitter**
*The latest Tweets from Cluster Truck (@clustertruck). The best way to get menus, schedules, and track LA food trucks in…*twitter.com](https://twitter.com/clustertruck)

[**FoodTruckr (@FoodTruckr) | Twitter**
*The latest Tweets from FoodTruckr (@FoodTruckr). The #1 online business resource for current and aspiring food truck…*twitter.com](https://twitter.com/foodTruckr)

[**NOLA Food Trucks (@NOLAfoodtrucks) | Twitter**
*The latest Tweets from NOLA Food Trucks (@NOLAfoodtrucks). In a city where food is vital + eating is often the most…*twitter.com](https://twitter.com/NOLAfoodtrucks)

In my research this weekend, I [learned](http://foodtruckr.com/2013/10/what-i-wish-id-known-before-starting-my-food-truck/) that most food truck owners work 10 hours a day, 7 days a week. Some do this as a passion for food, but many do it just to get by.

By far, food trucks make the [most money from catering](http://foodtruckr.com/2013/10/what-i-wish-id-known-before-starting-my-food-truck/). So if you, or your group, can afford the few thousand it would take to cater a truck on Election Day, please do. Free wasn’t my initial goal, but like in most industries, money talks. This is an uncertain venture, so many trucks will likely stick with their standard locations on Election Day. I don’t blame them. I would think that most districts have at least one donor in the region willing to sponsor a truck. I so need to accelerate a real web site in order to collaborate on this, if you know what an if-statement is please reach out on [GitHub](https://github.com/tacotrucktuesday/tacotrucktuesday.github.io/issues).

The next two months are going to be a whirlwind. If you can dedicate any amount of time to the project, it would be greatly appreciated. We need everything from design, to copy, social media, and press expertise. Coding will be only the beginning.

As always, point all vendors to [tacotrucktuesday.org](http://tacotrucktuesday.org), as that will be their place to organize and get more information.
