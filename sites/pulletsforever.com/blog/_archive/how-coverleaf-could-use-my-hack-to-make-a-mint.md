---
title: "How CoverLeaf could use my hack to make a mint"
date: 2010-08-05T05:26:17.730Z
tags: [hacks, web]
---

Sunday I exposed a major flaw in CoverLeaf’s web app magazine reader that allowed anyone to read most issues of the magazines they publish digitally for free.

But the real story is how CoverLeaf should turn the PDFs into a real revenue stream in the post iPad world. Basically they should provide PDFs to iPad users today, and market themselves as a solution for magazines that don’t want, or can’t use [Adobe’s Digital Publishing](http://www.adobe.com/digitalpublishing/) tools.

A lot of smaller, independent magazines have issues (pun intended) with getting a full-time developer to write scripts, shoot video, and such that a full iPad Application (even Adobe assisted) would require. While I am sure Adobe will eventually come out with a simplified solution for these publishers, I don’t see the original maker magazine, [Countryside & Small Stock Journal](http://www.countrysidemag.com/), updating their workflow anytime soon.

CoverLeaf is in the unique position of having the digital distribution rights (I assume) of their member magazines. As such they can offer iPad distribution of the PDF’s really simply:

1. Secure the PDF downloads with username and password

1. Create a RSS feed pointing to the now secured content

1. Promote the RSS feed to iOS and other tablet computers

While magazines like [WIRED](http://www.wired.com/) and maybe even [MAKE](http://makezine.com) will gravitate toward Adobe’s Digital Publishing tools, smaller magazines will be grateful for the added exposure CoverLeaf’s service provides.
