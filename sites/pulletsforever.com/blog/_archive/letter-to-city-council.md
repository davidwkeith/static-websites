---
title: Letter to City Council
date: 2006-02-13T04:48:26.142Z
tags: [chickens]
---

City of New Auburn, City Council
C/O Wendy Becker
8303 Eighth Avenue, PO Box 127
New Auburn, Minnesota 55366–0127

City Council:

I received your letter regarding my alleged violation of the anti-livestock ordinance within city limits. As the animals in question are not ‘Traditional farm animals raised for food’ but are non-traditional breeds chosen primarily for pest control in my organic garden. Thus, they would not legally qualify as livestock as defined in the city ordinances, or Minnesota Statutes, Chapter 17a (Livestock) and Chapter 31a (Meat and Poultry.)

Poultry is a generic term for fowl ‘raised for food,’ what I have on my property are pullets which are more costly than poultry as they are sexed to keep out the dominant males. This make them ideal for small communities as they are neither aggressive nor noisy. They are working animals that eat the copious grubs, mosquitoes, and small rodents that roam through my yard killing vegetation. This improves the community by making outdoor living more comfortable in the summer. The animals as I use them can only be considered working animals under the law in the same way a horse that pulls a plow, or bees which pollinate fruit trees are. Like the bees, pullets will provide an edible product as a byproduct of their work in the garden.

On the other hand, if one of my neighbors is complaining about any noise, smell, or other pollution created by the animals on my property I will be more than willing to work out a solution that would alleviate that concern while protecting my right to have animal labor tend my garden.

I will be more than willing to discuss my point of view at the next city council meeting if there are further questions in this matter. I would also like to receive a complete copy of the city ordinances for future reference.

Thank You,

David W. Keith
