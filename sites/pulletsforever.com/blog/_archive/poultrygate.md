---
title: PoultryGate
date: 2006-02-18T04:47:45.618Z
tags: [chickens]
---

An expert in dealing with the city of Marshfield, Massachusetts; my grandfather e-mailed me the following advice:

* Council members will want a face for the letter… it was a good idea to offer to meet with them

* Be humble, approach them as a person who does not no too much. Maintain your cool as you are trying to depolarize them and make them feel sympathetic

* Talk to the council’s clerk. In most municipalities the clerk is the dominate person for board actions. Ask her about how to proceed to get a waiver.

* Seek out an ‘old timer’ in town who can assist in mediating the issue.

He then provided two options to argue to city council:

Option A:

Go hat in hand to the City Council and ask for a waiver and work the following in to your argument:

* You are using your pet pullets to protect the ground water (environment from pesticides et al)

* In its broadest interpretation the ordinance can be used to prevent school children from husbandry science fair projects.

* Pet stores sell chickens (baby as pets) how are pullets interpreted by the ordinance in [this](http://www.birdhobbyist.com/articles/BirdHobbyist/Species/PetChickens.html) case?

Option B:

* Did the council member recluse herself from all council discussion and action before, during and after the incident? *Discretely as to look at the council minutes to see who was participating in the discussion and who directed the clear to write the letter for the council.*

* Does the state of Minnesota have a conflict of interest law that prohibits using ones political position to influence government action?

Of note:

* Pet stores do not sell chickens in the area (I got mine at Running’s Fleet & Farm in town) but they are widely regarded as [pets](http://www.birdhobbyist.com/articles/BirdHobbyist/Species/PetChickens.html) when not eaten.

* Conflict of interest laws tend to be in the realm of monetary or personal gain. While I could argue it would be a personal gain for her son not to loose his dog, there are in fact chicken farms just outside of town that are closer to her house than my chickens. Also, under Minnesota law if I feel that I or my personal property are threatened by the loose dog I can shoot it. Thus all they were admitting to is that the chickens were my property damaged by their dog. This would be the same as a auto accident where I was not at fault but had not insurance. I would be fined for not having insurance, but the other person would still be required to have their insurance company pay for damages. (Though without and an insurance company I would probably need a lawyer to see any money…)

Lastly my mother asks whether I can argue unequal enforcement of the law. It looks like I can as at least two other homes have ducks, and there are a few rabbits. In all cases I know these are raised as pets, but the ducks would still lay eggs so they are very similar.
