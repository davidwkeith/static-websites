---
title: Gingerbread Mac
date: 2002-12-23T04:49:24.215Z
tags: [food]
---

I needed some holiday cookies for the people who were working Christmas eve…

My girlfriend at the time had the idea to make a gingerbread Mac. She did all the work of course.

![Mac Classic made from gingerbread](/blog/_archive/gingerbread-mac/classic-gingerbread.jpg "Classic Gingerbread")

![Closeup of Apple Logo made from a Fruit Rollup](/blog/_archive/gingerbread-mac/fruit-rollup-logo.jpg "Fruit Rollup Logo")

![Closeup of a power cord made from gummies](/blog/_archive/gingerbread-mac/gummy-power-cord.jpg "Gummy Power Cord")
