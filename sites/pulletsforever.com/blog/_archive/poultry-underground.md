---
title: Poultry Underground
date: 2006-03-14T04:46:53.388Z
tags: [chickens]
---

Yesterday I received a postcard in the mail inviting me to subscribe to a new publication: Backyard Poultry. It sounded fun but I didn’t think much of it until this morning when I noted the cover article of the premiere issue “Chickens in the City: Local Ordinance Passed.” I quickly went to the Backyard Poultry website and read up on the Mad City Chickens Political Movement! All this time I thought I alone am fighting city council to let me keep pullets when in fact in the next state over there is a whole underground poultry movement!

I also spent much time at the [Mad City Chickens](http://madcitychickens.com/) website where I learned how a group of upstanding citizens petitioned the city council of Madison, Wisconsin to change a city ordinance to allow pullets in town!
