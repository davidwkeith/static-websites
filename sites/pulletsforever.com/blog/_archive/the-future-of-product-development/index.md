---
title: "The Future of Product Development"
date: 2019-04-23T18:55:53.142Z
description: "How I came to work at Tara AI"
tags: [startups]
---
![A team photo from my first week at Tara AI](./team-photo.jpg)

Frustrated with the state of product development solutions for software, I started [DevSweet](/leaving-the-nest) with [Cari Templeton](https://twitter.com/cariforcouncil) two years ago. However, a few months into research we were without a convincing go-to-market strategy and decided to end the project. Cari went on to help political candidates and I started working at a startup.

At this new startup I saw all the same issues with product development I had seen at Apple, Nest, and Google, but this time with far more at stake since we were still building the company’s first product. I started thinking again about the ideal product development toolchain and discovered that [Tara AI](https://tara.ai) had similar ideas.

I spent some time playing with the free version of the product, and while it is early days, I could see a lot of ideas similar to those I had for DevSweet were already implemented. I met up with the co-founders, [Syed](https://www.linkedin.com/in/syedahmedz/) and [Iba](https://www.linkedin.com/in/ibamasood/), and got a deep dive into the product and the vision. I was sold and over the next few months we continued to talk about the future and how I could help. In April I joined to lead engineering.

@[A video of Iba giving a talk at the Women in Tech Festival 2017](https://www.youtube-nocookie.com/embed/Q2MFQXkDMYo "Women in Tech Festival 2017: The Future of Work - Woman and Machine Symbiosis")

Tara AI has already hired a diverse team of engineers and located the company in the center of San Jose’s revitalized downtown. Being downtown means that there are hundreds of [interesting lunch options](https://www.yelp.com/search?find_desc=lunch&find_loc=75%20W%20Santa%20Clara%20St%2C%20San%20Jose%2C%20CA%2095113&ns=1&l=g%3A-121.901979446%2C37.3268981241%2C-121.876401901%2C37.3473708175), museums, and more, all within walking distance. I live a few miles away and bike into work every day, while others take transit or even drive in, whatever works. One-to-one meetings are often an excuse to get out of the office with a [Chromatic Coffee](https://www.chromaticcoffee.com/) in hand, exploring the city around us. Ultimately Tara AI’s headquarters is the opposite of the sprawling office parks that have dominated Silicon Valley for decades.

A great product, healthy office setup, and diverse team have all set the stage for success. The team is constantly bouncing ideas off each other and improving on the original vision. While still being scrappy, we are focused on best practices, and encoding those into the product for our customers. We practice the company’s vision, and are hiring inline with that vision. We want people who are not just excellent coders, but also passionate about using technology to help remove bias from software engineering practices so we can all build better products faster.

We are just getting started making this vision a reality and have a ton of open positions ranging from engineering to product and sales. To learn more, visit our [website](https://tara.ai), [Angel Job Board](https://angel.co/tara-ai/jobs), ask a question below, or simply email [dwk@tara.ai](mailto:dwk@tara.ai).
