---
title: "Leaving the Nest"
date: 2017-06-21T00:31:01.191Z
tags: [startups]
---

Yesterday the engineering team at Nest got quite a shock when an email went out informing the team of my departure at the end of June, never an easy thing to tell your co-workers. I have been at the company for over 5 and a half years, and have been involved in almost every level of the company, from hardware patents to marketing site hacks. Like many early Nesters, I had many roles over the years and rarely turned down a challenging project.

At Nest we hired and developed the best team I have ever worked with. Truly top notch software engineers, and outstanding people with big hearts, and a passion for the product and mission. But having a highly functional team lead to some issues for me as a manager: the team is able to run the day-to-day operations on their own with little oversight. I converted a few individual contributors to managers over the years, and became one of the dreaded middle managers. (my team says otherwise though) This gave me some time to think about what else might be next.

I came to the realization that one of my passions is quality tooling, and many tools for software development are unloved, low quality products. A large group of these tools are either in-house side projects or “enterprise” software that is one-size-fits-none. All require a ton of custom engineering to get up and running, and many have ugly and confusing user interfaces with lots of performance problems and obvious features missing.

Thus, I am going to take what I have learned developing software for the last 20 years and use it to fix the unloved developer tools we use everyday. I want to make it so our tools just work and software developers can spend less time configuring tools and focus on what they do best: building great software for end users.

Over the summer I am going to collect my thoughts, seek financing, and if all goes well, start solving these problems. In the meantime I would love to hear about the tools you use, what you love, and what could be better.
