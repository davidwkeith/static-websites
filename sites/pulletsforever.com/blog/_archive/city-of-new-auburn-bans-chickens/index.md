---
title: City of New Auburn bans Chickens
date: 2006-02-11T04:48:26.142Z
tags: [chickens]
---
![Letter from City Council of the New Auburn, MN](letter.png)

This morning I checked my mail to find the above letter in the mailbox. The ordinance in question is Article 4, Section 402.10 which covers permitted use of property in the R1 — General Residential Zone. The section reads “CONDITIONAL USES: Farming, excluding the raising of livestock.” They also send a list of definitions defining Livestock as “Traditional farm animals raised for food including, but not limited to cattle, swine, sheep, poultry and rabbits.”
