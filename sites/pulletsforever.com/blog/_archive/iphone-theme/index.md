---
title: iPhone Theme
date: 2003-08-31T04:49:04.477Z
tags: [hacks]
---
![animation of theme](iphone-theme.gif)

I created a theme file for my Sony Ericsson phone to give it the Mac OS X look and feel. It will work with any T600 series phone, the instructions for installation are available on Page 16 of the T610 Manual.

|          |                        |
|----------|------------------------|
| Download | [iPhone.zip][download] |

[download]: /blog/_archive/iphone-theme/iPhone.zip