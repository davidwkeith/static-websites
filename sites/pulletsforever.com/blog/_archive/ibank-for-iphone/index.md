---
title: iBank for iPhone
date: 2008-02-08T05:49:20.983Z
tags: [hacks, ios, web]
---
![iBank on iPhone](ibank-iphone.png)

**UPDATE:** IGG Software has released iBank Mobile for the iPhone. It can be purchased through [iTunes](https://itunes.apple.com/us/app/banktivity-for-iphone-formerly/id919518925?mt=8).

All data stored securely on your [MobileMe iDisk](http://me.com/) for syncing with iBank from your computer.

* iPhone-like user interface using [iUI](http://code.google.com/p/iui/).

* Add, edit, and delete transactions on the go.

* View balances.

* Automatically memorizes new transactions.

* Uses the memorized transactions from your Mac to speed data entry on the go.
