function updateTime() {
  let clockDiv = document.getElementById("crontab-clock");
  let now = new Date();
  let cronString = [
    now.getMinutes(),
    now.getHours(),
    now.getDate(),
    now.getMonth() + 1,
    now.getDay(),
  ].join(" ");
  clockDiv.innerText = cronString;
}
updateTime();
window.setInterval(updateTime, 1000);
