import globalSchema from "../../../data/schema.js";

export default {
  ...globalSchema,
  name: "crontab clock",
  description: "The current time in crontab format.",
  image: "https://crontab.dwk.io/favicon.png",
  url: "https://crontab.dwk.io",
};
