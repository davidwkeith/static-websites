import globalMetadata from "../../../data/metadata.js";

export default {
  ...globalMetadata,
  title: "crontab clock",
  site: "crontab.dwk.io",
  url: "https://crontab.dwk.io",
  favicon: "/favicon.png",
  logo: {
    src: "/favicon.png",
    alt: "Terminal.app icon",
  },
  description: "The current time in crontab format.",
  hasRSSFeed: false,
  cloudflare_insights: {
    token: "b87a1fee3c144014a1716354e93e8d53",
  },
};