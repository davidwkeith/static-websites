import globalSchema from "../../../data/schema.js";

globalSchema.author.email = "mailto:david@davidandshawna.us";

export default {
  ...globalSchema,
  name: "David and Shawna",
  description: "David and Shawna's wedding website.",
  image: "https://davidandshawna.us/images/favicon.png",
  url: "https://davidandshawna.us/",
};
