import globalMetadata from "../../../data/metadata.js";

export default {
  ...globalMetadata,
  title: "David and Shawna",
  site: "davidandshawna.us",
  favicon: "/images/favicon.png",
  logo: {
    src: "/images/favicon.png",
    alt: "Tent icon",
  },
  cloudflare_insights: {
    token: "b72f199aa3f348de83880e75ae9c3bad",
  },
};
