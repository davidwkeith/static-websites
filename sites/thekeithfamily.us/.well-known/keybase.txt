==================================================================
https://keybase.io/dwkeith
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://thekeithfamily.us
  * I am dwkeith (https://keybase.io/dwkeith) on keybase.
  * I have a public key ASD4Svhb_GwqT-WZVfHxwCksTXXOgifKGcGEBmEv2C0fIQo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "01014101d6366d94379f9fa945e47d0253b3aa8efb935f6bbdc4f0b6cf52bd84868d0a",
      "host": "keybase.io",
      "kid": "0120f84af85bfc6c2a4fe59955f1f1c0292c4d75ce8227ca19c18406612fd82d1f210a",
      "uid": "e9797f5b266ccbbae424323d29ce3819",
      "username": "dwkeith"
    },
    "merkle_root": {
      "ctime": 1715908321,
      "hash": "3d675a87245cb368c72023e489ad91f039a67ceb8c9e4ba29e58c8eda214eda35a6e92bdd560fe426a3f81e32822a4ce293c7999c63d7bb29b5d8cc646e06813",
      "hash_meta": "c6eefb4b453b06367d69c916cc34ac1046103ff1dc22bd118fd7f062f5e27202",
      "seqno": 25732206
    },
    "service": {
      "entropy": "NlcOi3pii+nR5rjr+VKQE144",
      "hostname": "thekeithfamily.us",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "6.2.8"
  },
  "ctime": 1715908512,
  "expire_in": 504576000,
  "prev": "b0d225561ddb45d5db16de42ac903a081c8b39ae1aba290a2bf711af6ca30bc9",
  "seqno": 101,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg+Er4W/xsKk/lmVXx8cApLE11zoInyhnBhAZhL9gtHyEKp3BheWxvYWTESpcCZcQgsNIlVh3bRdXbFt5CrJA6CByLOa4auikKK/cRr2yjC8nEIF08Wo9pigxYTi5pNMy5zv15PKr7pvBoEmfI+2HuZKcZAgHCo3NpZ8RAhpxIHx0xqZMuIUQ4STBb/GMTd8dp/qKmcyJSY5OO5Fj/Kd8mAHFhs6DRWxWgPQeQ1u9wYg4+1OjSQYDU1MzYCahzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIKYuggGccCvwAU2XdcHOPITG/XJeO1LBHHQYrXF1Wjl8o3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/dwkeith

==================================================================