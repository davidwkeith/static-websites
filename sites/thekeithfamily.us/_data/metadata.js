import globalMetadata from  "../../../data/metadata.js";

export default {
  ...globalMetadata,
  title: "The Keith Family",
  site: "thekeithfamily.us",
  url: "https://thekeithfamily.us",
  favicon: "/img/keith-clan.png",
  logo: {
    src: "/img/keith-clan@2x.png",
    alt: "Keith Clan Tartan",
  },
  description: "The Keith Family Website",
  cloudflare_insights: {
    token: "db3be463c5a743759588c01faaa8e277",
  },
};
