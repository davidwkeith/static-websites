import globalSchema from "../../../data/schema.js";

globalSchema.author.email = "mailto:dwk@thekeithfamily.us";

export default{
  ...globalSchema,
  name: "The Keith Family Website",
  description: "David and Shawna's wedding website.",
  image: "https://thekeithfamily.us/img/keith-clan@2x.png",
  url: "https://thekeithfamily.us/",
};
