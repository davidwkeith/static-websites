# Static Sites

Static sites created by [David W. Keith](http://dwk.io).

Builds should be triggered on changes to

- `includes/* sites/{site}}/*`

## TODO

- [ ] Fix CSS loading
- [ ] Use reverse domain names for site directories
- [ ] Schema.org
- [ ] Headers & code-signing
- [ ] Style RSS, see https://rknight.me/blog/styling-rss-and-atom-feeds/
- [ ] Markdown It 
  - `@mdit/plugin-abbr`
  - `@mdit/plugin-alert`
  - `@mdit/plugin-dl`
  - `@mdit/plugin-figure`

## Site Notes

### dwk.io (me)

- [webmentions](https://monocle.p3k.io/channel/notifications)
- [Ecograder](https://ecograder.com/report/iHs6JCLdDhkvX7rtfo78e3xk)
- [PagesSpeed Insights](https://pagespeed.web.dev/analysis/https-dwk-io/jsu0k4sh6h)

#### crontab.dwk.io

### pulletsforever.com (blog)

- [PagesSpeed Insights](https://pagespeed.web.dev/analysis/https-pulletsforever-com/cgkfdljxgz)
