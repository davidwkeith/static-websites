import author from "./author.js";
import head_links from "./head_links.js";
import schema  from "./schema.js";

export default {
  language: "en",
  ogType: "website",
  hasRSSFeed: true,
  colorScheme: "dark light",
  author,
  head_links,
  schema,
};
