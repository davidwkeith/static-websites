export default {
  "@context": "https://schema.org",
  "@type": "WebSite",
  author: {
    "@type": "Person",
    name: "David W. Keith",
    url: "https://dwk.io",
    email: "mailto:me@dwk.io",
  },
};