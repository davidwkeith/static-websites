export default {
  "base-uri": ["self"],
  "font-src": ["self"],
  "img-src": ["self", "mirrors.creativecommons.org"],
  "script-src-elem": [
    "self",
    "https://static.cloudflareinsights.com/beacon.min.js",
    "https://static.cloudflareinsights.com/beacon.min.js/*",
  ],
  "style-src-elem": ["self", "sha256", "unsafe-inline"],
};