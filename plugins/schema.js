// TIOD: Validate the Schema.org JSON-LD before returning it

export default function (eleventyConfig) {
  eleventyConfig.addShortcode("GenerateSchema", async function (schema) {
      return JSON.stringify(schema);
    }
  );
};
